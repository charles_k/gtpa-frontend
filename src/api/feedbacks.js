import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'feedbackService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getFeedbacks() {
    this.setToken()
    let url = config.FEEDBACK_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  sendFeedback(feedback) {
    this.setToken()
    let url = config.FEEDBACK_URL
    return axios.post(url, feedback)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updateFeedback(feedback) {
    this.setToken()
    let url = config.FEEDBACK_URL + '/' + feedback.id
    return axios.put(url, feedback)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removeFeedback(id) {
    this.setToken()
    let url = config.FEEDBACK_URL + '/' + id
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }
}