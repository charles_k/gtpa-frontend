import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'authService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  authenticate(request) {
    return axios.post(config.LOGIN_URL, request)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  register(request) {
    let url = config.REGISTER_URL
    return axios.post(url, request)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  setUser() {
    this.setToken()
    let url = config.LOGGED_IN_USER_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  logout(token) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    let url = config.LOGOUT_USER_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  validAuth(loggedIn) {
    if (loggedIn == true) {
      return true
    } else {
      return false
    }
  }
}