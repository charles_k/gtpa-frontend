import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'eventService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getEvents() {
    this.setToken()
    let url = config.EVENTS_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getEvent(eventId) {
    this.setToken()
    let url = config.EVENTS_URL + '/' + eventId
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  addEvent(event) {
    this.setToken()
    let url = config.EVENTS_URL
    return axios.post(url, event)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updateEvent(event) {
    this.setToken()
    let url = config.EVENTS_URL + '/' + event.id
    return axios.put(url, event)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removeEvent(id) {
    this.setToken()
    let url = config.EVENTS_URL + '/' + id
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }
}