import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'categoryService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getCategories() {
    this.setToken()
    let url = config.CATEGORIES_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getCategory(catId) {
    this.setToken()
    let url = config.CATEGORIES_URL + '/' + catId
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  addCategory(category) {
    this.setToken()
    let url = config.CATEGORIES_URL
    return axios.post(url, category)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updateCategory(category) {
    this.setToken()
    let url = config.CATEGORIES_URL + '/' + category.id
    return axios.put(url, category)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removeCategory(id) {
    this.setToken()
    let url = config.CATEGORIES_URL + '/' + id
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }
}