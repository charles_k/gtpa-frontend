import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'conversationService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getConversations(userinfo) {
    this.setToken()
    let url = config.CONVERSATION_URL
    return axios.post(url, userinfo)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }
}

