import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'mentorService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getMentors() {
    let url = config.MENTORS_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getMentorByiD(mentorId) {
    let url = config.MENTORS_URL + '/' + mentorId
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getMentorRequest(mentorId) {
    this.setToken()
    let url = config.MENTORS_URL + '/' + mentorId + '/request'
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getMentorFeedbacks(mentorId) {
    let url = config.MENTORS_URL + '/' + mentorId + '/feedback'
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  addMentor(mentor) {
    this.setToken()
    let url = config.MENTORS_URL
    return axios.post(url, mentor)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updateMentor(mentor) {
    this.setToken()
    let url = config.MENTORS_URL + '/' + mentor.id
    return axios.put(url, mentor)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removeMentor(mentorId) {
    this.setToken()
    let url = config.MENTORS_URL + '/' + mentorId
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }

}
