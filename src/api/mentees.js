import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'menteeService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getMentees() {
    this.setToken()
    let url = config.MENTEES_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getMenteesByiD(menteeId) {
    this.setToken()
    let url = config.MENTEES_URL + '/' + menteeId
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getMenteeRequest(menteeId) {
    this.setToken()
    let url = config.MENTEES_URL + '/' + menteeId + '/request'
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getMenteeFeedbacks(menteeId) {
    let url = config.MENTEES_URL + '/' + menteeId + '/feedback'
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  addMentee(mentee) {
    this.setToken()
    let url = config.MENTEES_URL
    return axios.post(url, mentee)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updateMentee(mentee) {
    this.setToken()
    let url = config.MENTEES_URL + '/' + mentee.id
    return axios.put(url, mentee)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removeMentee(menteeId) {
    this.setToken()
    let url = config.MENTEES_URL + '/' + menteeId
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }

}
