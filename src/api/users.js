import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'userService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getUsers() {
    this.setToken()
    let url = config.USERS_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getUserByiD(userId) {
    this.setToken()
    let url = config.USERS_URL + '/' + userId
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getUserMentor(userId) {
    this.setToken()
    let url = config.USERS_URL + '/' + userId + '/mentors'
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getUserMentee(userId) {
    this.setToken()
    let url = config.USERS_URL + '/' + userId + '/mentees'
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  addUser(user) {
    this.setToken()
    let url = config.USERS_URL
    return axios.post(url, user)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updateUser(user) {
    this.setToken()
    let url = config.USERS_URL + '/' + user.id
    return axios.put(url, user)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removeUser(userId) {
    this.setToken()
    let url = config.USERS_URL + '/' + userId
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }

}
