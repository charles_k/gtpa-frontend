import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'postsService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getPosts() {
    this.setToken()
    let url = config.POSTS_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  getPost(postid) {
    this.setToken()
    let url = config.POSTS_URL + '/' + postid
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  addPost(post) {
    this.setToken()
    let url = config.POSTS_URL
    return axios.post(url, post)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  updatePost(post) {
    this.setToken()
    let url = config.POSTS_URL + '/' + post.id
    return axios.put(url, post)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  },

  removePost(id) {
    this.setToken()
    let url = config.POSTS_URL + '/' + id
    return axios.delete(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }
}