import Vue from 'vue'
import is from 'is_js'
import axios from 'axios'
import config from '../config'

Vue.use(axios)

export default {
  name: 'reportsService',

  setToken() {
    let vuex = JSON.parse(localStorage.getItem('vuex'))
    if (is.not.null(vuex)) {
      let token = vuex.auth.token
      if (token !== '') {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
      }
    }
  },

  getDashboardReports() {
    this.setToken()
    let url = config.REPORTS_URL
    return axios.get(url)
      .then((response) => Promise.resolve(response.data))
      .catch((error) => Promise.reject(error))
  }
}

