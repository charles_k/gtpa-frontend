import Vue from 'vue';
import mixin from './mixins';
import eventbus from "./eventbus";

import '@/plugins/element/index.js'
import '@/assets/css/app.css'
import '@/assets/css/style.css'
import '@fortawesome/fontawesome-free/css/all.css'

import App from './App.vue'
import router from './router'
import { store } from './store/'

Vue.use(require('vue-moment'));

Vue.mixin(mixin)
Vue.config.productionTip = false
Vue.prototype.$eventbus = eventbus;

import '@/plugins/pusher/index.js'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
