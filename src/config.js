function getApiUrl() {
  let hn = window.location.hostname
  if (hn === 'localhost') {
    return 'http://127.0.0.1:8000/api/'
  } else if (hn === 'gtpa.netlify.app') {
    return 'https://www.thomqdesigns.com/api/'
  }
  return 'https://www.thomqdesigns.com/api/'
}

const API_URL = getApiUrl();

export default {
  CONVERSATION_URL: API_URL + 'conversation',
  FEEDBACK_URL: API_URL + 'feedbacks',
  USERS_URL: API_URL + 'users',
  MENTORS_URL: API_URL + 'mentors',
  MENTEES_URL: API_URL + 'mentees',
  MESSAGES_URL: API_URL + 'messages',
  REPORTS_URL: API_URL + 'dashboard_reports',
  REQUESTS_URL: API_URL + 'requests',
  POSTS_URL: API_URL + 'posts',
  EVENTS_URL: API_URL + 'events',
  CATEGORIES_URL: API_URL + 'categories',
  LOGIN_URL: API_URL + 'login',
  REGISTER_URL: API_URL + 'register',
  LOGGED_IN_USER_URL: API_URL + 'currentuser',
  LOGOUT_USER_URL: API_URL + 'logout'
}