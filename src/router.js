import Vue from "vue";
import Router from "vue-router";
import is from "is_js";
import { store } from "./store/";
import authService from "./api/auth";

// user/public routes
import MainPage from "./views/MainPage";
import HomeEvent from "./views/Events";
import EventSingle from "./views/EventSingle";
import HomeBlog from "./views/Blog";
import BlogSingle from "./views/BlogSingle";
import Home from "./views/Home";
import Login from "./views/auth/Login";
import Register from "./views/auth/Register";
import Welcome from "./views/Welcome";
import Profile from "./views/Profile";
import Account from "./views/Account";
import Mentor from "./views/Mentor";
import UserMentorsProfile from "./views/AllUsersMentorProfile";

// User dashboard
import UserDashboard from "./views/dashboard/UserDashboard";
import Mentorship from "./views/Mentorship";
import UserRequests from "./views/Requests";
import UserMessages from "./views/Messages";
import Feedback from "./views/Feedback";

// admin dashboard
import AdminDashboard from "./views/dashboard/AdminDashboard";
import AdminLogin from "./views/dashboard/AdminLogin";
import Dashboard from "./views/dashboard/Dashboard";
import Mentors from "./views/dashboard/Mentors";
import Mentees from "./views/dashboard/Mentees";
import Requests from "./views/dashboard/Requests";
import Users from "./views/dashboard/Users";
import AdminFeedback from "./views/dashboard/AdminFeedback";
import Events from "./views/dashboard/Events";
import Blog from "./views/dashboard/Blog";

Vue.use(Router);

let routes = [
  {
    path: "/",
    redirect: "/home",
    name: "Main",
    component: MainPage,
    children: [
      {
        path: "/home",
        name: "Home",
        component: Home
      },
      {
        path: "/events",
        name: "homeEvent",
        component: HomeEvent
      },
      {
        path: "/events-single",
        name: "Single Event",
        component: EventSingle
      },
      {
        path: "/blog",
        name: "homeBlog",
        component: HomeBlog
      },
      {
        path: "/single-blog-news",
        name: "Single blog",
        component: BlogSingle
      },
      {
        path: "/login",
        name: "Login",
        component: Login
      },
      {
        path: "/register",
        name: "Register",
        component: Register
      },
      {
        path: "/welcome",
        name: "Welcome",
        component: Welcome
      },
      {
        path: "/settings",
        name: "account",
        component: Account
      },
      {
        path: "/profile",
        name: "Profile",
        component: Profile
      },
      {
        path: "/mentors",
        name: "mentor",
        component: Mentor
      },
      {
        path: "/user-mentors/:id",
        name: "user-mentors-profile",
        component: UserMentorsProfile
      }
    ]
  },
  {
    path: "/user-dashboard",
    redirect: "/user-dashboard/mentorship",
    name: "UserDashboard",
    component: UserDashboard,
    meta: { requiresAuth: true },
    children: [
      {
        path: "mentorship",
        name: "mentorship",
        component: Mentorship
      },
      {
        path: "user-requests",
        name: "user request",
        component: UserRequests
      },
      {
        path: "messages",
        name: "Message",
        component: UserMessages
      },
      {
        path: "feedback",
        name: "Feedback",
        component: Feedback
      }
    ]
  },
  {
    path: "/admin-login",
    name: "AdminLogin",
    component: AdminLogin
  },
  {
    path: "/admin",
    name: "Admin Dashboard",
    redirect: "/admin/dashboard",
    component: AdminDashboard,
    meta: { requiresAuth: true },
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard
      },
      {
        path: "mentors",
        name: "Mentors",
        component: Mentors
      },
      {
        path: "mentees",
        name: "Mentees",
        component: Mentees
      },
      {
        path: "requests",
        name: "requests",
        component: Requests
      },
      {
        path: "users",
        name: "Users",
        component: Users
      },
      {
        path: "feedback",
        name: "admin-feedback",
        component: AdminFeedback
      },
      {
        path: "all-events",
        name: "Events",
        component: Events
      },
      {
        path: "blogs",
        name: "Blog",
        component: Blog
      }
    ]
  }
];

const router = new Router({
  mode: "hash",
  base: process.env.BASE_URL,
  linkActiveClass: "open active",
  routes
});

router.beforeEach((to, from, next) => {
  let openViews = [
    "AdminLogin",
    "Home",
    "Login",
    "Register",
    "mentor",
    "homeBlog",
    "homeEvent",
    "user-mentors-profile",
    "Single Event",
    "Single blog"
  ];
  let canAccess =
    is.inArray(to.name, openViews) ||
    authService.validAuth(store.getters.userLoggedIn);
  return canAccess ? next() : next("/");
});

export default router;
