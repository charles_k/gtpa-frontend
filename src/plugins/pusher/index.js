import Pusher from 'pusher-js'
import eventbus from "@/eventbus";

let pusher = new Pusher('09b3242a40cf325934b9', {
  cluster: 'mt1'
});

let channel = pusher.subscribe('chat');

channel.bind('MessageSent', function(data) {
  eventbus.$emit('receiving_message', data.message);
});