import { Message } from "element-ui";
import userService from '../api/users';

export default {
  components: { Message },
  methods: {
    getuserAge(dob) {
      let age = new Date(dob)
      var diff_ms = Date.now() - age.getTime();
      var age_dt = new Date(diff_ms);

      return Math.abs(age_dt.getUTCFullYear() - 1970);
    },
    getUserTypeTag(type) {
      let color;
      switch (type) {
        case 'Super Admin':
          color = 'danger'
          break;
        case 'Admin':
          color = 'primary'
          break;
        case 'Editor':
          color = 'warning'
          break;
        case 'Mentor':
          color = 'primary'
          break;
        case 'Mentee':
          color = 'success'
          break;
      }
      return color
    },
    saveImageToServer(img, userId, updateField) {
      let updateForm = {};
      updateForm.id = userId;
      updateForm.pic = img
      userService
        .updateUser(updateForm, updateField)
        .then((response) => {
          updateField = response.user.pic;
          this.successMessage('Success', 'Profile pic updated successfully');
        })
        .catch(() =>
          this.errorMessage('Errors', 'User picture failed to update (User id check)')
        );
    },
    setProfilePic(pic) {
      if (pic !== "" || pic !== null) {
        return pic
      } else {
        return "https://via.placeholder.com/150"
      }
    },
    setPic(pic) {
      if (pic !== null) {
        return pic
      } else {
        return "https://via.placeholder.com/400x250"
      }
    },
    successMessage(title, message) {
      Message.success({
        title: title,
        message: message,
        duration: 2000
      });
    },
    errorMessage(title, message) {
      Message.error({
        title: title,
        message: message,
        duration: 3000
      });
    },
    adminLogout() {
      this.$store.dispatch("logout").then(() => {
        this.$router.push("/admin-login");
      });
    },
    logout() {
      this.$store.dispatch("logout").then(() => {
        this.$router.push("/login");
      });
    },
  }
};
